import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/config.dart';
import 'package:flutter_app/widget/Utils.dart';
import 'package:http/http.dart' as http;

class AboutPage extends StatefulWidget {

  AboutPage({Key key}): super(key: key);

  @override
  _AboutState createState() => _AboutState();

}

class _AboutState extends State<AboutPage> {

  final String about = "jvm123.com 是一个java技术分享站，内容涉及java、jvm、程序开发，"
      "单元测试框架Spock教程，测试视频教程，spring框架教程等，以及服务"
      "器搭建、linux、docker等相关技术。";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
//      appBar: AppBar(
//        title: Text("Java 技术分享站"),
//      ),
      body: Container(
        padding: EdgeInsets.all(0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(0),
              child: Image.asset("images/logo.png", width: 56, height: 48,),
            ),
            Container(
              padding: EdgeInsets.all(20),
              child: Column(
                children: <Widget>[
                  Text("Java技术分享站"),
                  Text("v 1.1.0"),
                ],
              )
            ),
            Container(
              padding: EdgeInsets.all(60),
              child: Column(
                children: <Widget>[
                  Divider(),
                  Stack(
                    children: <Widget>[
                      Align(
                        alignment: AlignmentDirectional.centerStart,
                        child: InkWell(
                          child: Text("功能"),
                          onTap: () {
                            Utils.showAlert(context, '功能', "博客、博客搜索、图库、图片标题描述信息的修改等。");
                          }
                        ),
                      ),
                    ],
                  ),
                  Divider(),
                  Stack(
                    children: <Widget>[
                      Align(
                        alignment: AlignmentDirectional.centerStart,
                        child: InkWell(
                            child: Text("关于"),
                            onTap: () {
                              Utils.showAlert(context, 'Java技术分享站', about);
                            }
                        ),
                      ),
                    ],
                  ),
                  Divider(),
                  Stack(
                    children: <Widget>[
                      Align(
                        alignment: AlignmentDirectional.centerStart,
                        child: InkWell(
                            child: Text("同步博客"),
                            onTap: () {
                              http.get("${Config.apiHost}/sync/all").then((response) =>
                                  Utils.showToast("同步完成！")
                              );
                            }
                        ),
                      ),
                    ],
                  ),
                  Divider(),
                  Stack(
                    children: <Widget>[
                      Align(
                        alignment: AlignmentDirectional.centerStart,
                        child: InkWell(
                            child: Text("检查新版"),
                            onTap: () {
                              Utils.showAlert(context, "", "恭喜你，已经是最新版本！");
                            }
                        ),
                      ),
                    ],
                  ),
                  Divider(),
                ],
              )
            ),
            Container(
                padding: EdgeInsets.all(40),
                child: Column(
                  children: <Widget>[
                    Text("jvm123.com", style: TextStyle(color: Config.bodyColor),),
                  ],
                )
            ),
          ],
        ),
      ),
    );
  }


}