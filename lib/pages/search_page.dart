import 'package:flutter/material.dart';

class SearchPage extends StatefulWidget {

  SearchPage({Key key}) : super(key: key);

  @override
  _MyFirstPageState createState() {
    return new _MyFirstPageState();
  }
}

class _MyFirstPageState extends State<SearchPage> {
  String title = "博客搜索";
  String wd = "空";

  void loadData() async {
//    http.Response response = await http.get(widget.url);
    setState(() {
//      Map postMap = json.decode(response.body);
//      title = postMap['postTitle'];
//      content = postMap['postContent'];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: TextField(
          onChanged: (value) {
            wd = value;
          },
          decoration: InputDecoration(
//            labelText: "搜索",
//            icon: Icon(Icons.search),
            suffixIcon: InkWell(
              child: Icon(Icons.search),
              onTap: () {
//                Utils.showToast(wd);
                Navigator.of(context).pop(wd);
              },
            ),
            hintText: "搜索任意关键词或语句",
          ),
          onSubmitted: (text) => Navigator.of(context).pop(wd),
        ),
      ),
    );
  }
}