
import 'package:flutter/material.dart';

class Config {

  static final String imgHost = "http://47.107.161.57";
  static final String apiHost = "http://api.jvm123.com";


  // colors
//  static final Color themeColor = Colors.greenAccent;
//  static final Color themeColor = Colors.indigoAccent;
//  static final Color themeColor = Colors.indigoAccent;
  static final Color themeColor = Colors.blue;
  static final Color grayColor = Colors.grey;

  static final Color titleColor = Colors.black54;
  static final Color bodyColor = Colors.black38;
  static final Color borderColor = Colors.black12;

}