
import 'package:flutter/services.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/widget/Utils.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class Browser extends StatelessWidget {

  final String title;
  final String url;
  final String shareUrl;

  final MethodChannel channel = const MethodChannel('flutter_share_me');

  Browser({Key key, @required this.title, @required this.url, this.shareUrl}):super(key: key);

  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
//      appBar: AppBar(
//        title: Text(title),
//        actions: <Widget>[
//          IconButton(
//            icon: Icon(Icons.share),
//            onPressed: () {
//              if(shareUrl == null || shareUrl == "") {
//                shareImage(url);
//              } else {
//                shareImage(shareUrl);
//              }
//            },
//          )
//        ],
//      ),
      url: url,
      withZoom: false,
      withLocalStorage: true,
      withJavascript: true,
      //本地缓存
      hidden: false,
      //默认状态隐藏
      initialChild: Container(
        color: Colors.white,
        child: Center(
          child: Text('jvm123.com 提醒您：精彩内容，马上呈现...'),
        ),
      ), //设置初始化界面

    );
  }

  shareImage(String url) async {
    dynamic result;
    try {
      result = await channel.invokeMethod('system', {"msg": url});
      Utils.showToast("分享成功！");
    } catch (e) {
      Utils.showToast("分享失败！");
    }
  }
}