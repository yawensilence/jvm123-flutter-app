
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/config.dart';
import 'package:flutter_app/widget/Utils.dart';
import 'package:http/http.dart' as http;

class EditImageInfo extends StatelessWidget {

  final Image img;
  final Map data;

  EditImageInfo({Key key, @required this.img, @required this.data}):super(key: key);

  @override
  Widget build(BuildContext context) {
    String updateUrl = "${Config.apiHost}/api/img/update?";

    String id = data['id'];
    String imgTitle = data['title'] ?? "no title";
    String content = data['content'] ?? "no description";

    return Scaffold(
      appBar: AppBar(
        title: Text("编辑信息"),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(20),
        child: Column(
          children: <Widget>[
            img,
            Text(""),
            Text(
              imgTitle,
              style: Theme.of(context).textTheme.title,
            ),
            TextField(
              onChanged: (value) {
                imgTitle = value;
              },
              decoration: InputDecoration(
                labelText: "名称",
                suffixIcon: InkWell(
                  child: Icon(Icons.subdirectory_arrow_left),
                  onTap: () {
                    http.post("${updateUrl}id=$id&title=$imgTitle")
                        .then((response) {
                          Utils.showToast("操作成功！${response.statusCode}");
                          Navigator.of(context).pop();
                    });
                  },
                ),
                hintText: data['title'] ?? "no title",
              ),
            onSubmitted: (text) {
              imgTitle = text;
            }),
            Text(""),
            Text(
              content,
              style: Theme.of(context).textTheme.body2,
            ),
            TextField(
              onChanged: (value) {
                content = value;
              },
              decoration: InputDecoration(
                labelText: "内容描述",
                suffixIcon: InkWell(
                  child: Icon(Icons.subdirectory_arrow_left),
                  onTap: () {
                    http.post("${updateUrl}id=$id&content=$content")
                        .then((response) {
                          Utils.showToast("操作成功！${response.statusCode}");
                          Navigator.of(context).pop();
                    });
                  },
                ),
                hintText: data['content'] ?? "no description",
              ),
              onSubmitted: (text) {
                content = text;
              }
            ),
          ],
        ),
      ),
    );
  }


}