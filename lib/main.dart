import 'package:flutter/material.dart';
import 'package:flutter_app/config.dart';
import 'package:flutter_app/pages/about_page.dart';

import 'pages/post_page.dart';
import 'pages/search_page.dart';
import 'pages/image_page.dart';
import 'pages/site_page.dart';

void main() {
  runApp(MaterialApp(
    home: MyNavPage(),
    routes: <String, WidgetBuilder> {
      "search": (BuildContext context) => SearchPage(),
    },
    theme: ThemeData(
      appBarTheme: AppBarTheme(
        color: Config.themeColor,
//        textTheme: TextTheme(
//          title: TextStyle(color: Colors.blue),
//        ),
      ),
    ),
  ));
}


class MyNavPage extends StatefulWidget {

  @override
  _MyNavPageState createState() {
    return new _MyNavPageState();
  }
}

class _MyNavPageState extends State<MyNavPage> {

  int _currentIndex = 0;
  var _controller = PageController(
    initialPage: 0,

  );


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: _controller,
        children: <Widget>[PostPage(), ImagePage(), SitePage(), AboutPage()],
        physics: AlwaysScrollableScrollPhysics(),
        onPageChanged: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
      ),
      bottomNavigationBar: BottomNavigationBar(
        unselectedItemColor: Config.grayColor,
        selectedItemColor: Config.themeColor,
        currentIndex: _currentIndex,
        onTap: (index) {
          _controller.jumpToPage(index);
          setState(() {
            _currentIndex = index;
          });
        },
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
            ),
            activeIcon: Icon(
              Icons.home,
            ),
            title: Text("博客"),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.image,
            ),
            activeIcon: Icon(
              Icons.image,
            ),
            title: Text("图库"),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.cloud_queue,
            ),
            activeIcon: Icon(
              Icons.cloud,
            ),
            title: Text("站点"),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.person_outline,
            ),
            activeIcon: Icon(
              Icons.person,
            ),
            title: Text("关于",),
          ),
        ],
      ),
    );
  }

}


